<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
require_once "juego.php";
class PDOJuegoRepository {
    function __construct(\PDO $conn) {
      $this->conn = $conn;
}
//query para obtener todos los datos de la base de datos, ordenados por la fecha en al que fueron añadidos a la base de datos 
function getAll(): array | null {
$stm = $this->conn->prepare("SELECT * from juegos ORDER BY fechaAdicion DESC");
  $stm->execute();
  $result = $stm->fetchAll();
  if ($result) {
    return $result;
  }else{
    return null;
  } }
//query para hacer la busqueda segun los filtros,con los juegos ordenados por la fecha en al que fueron añadidos a la base de datos,
//veremos los que fueron añadidos mas recientemente a la base de datos mas arriba 
function getbyallfilters(string $nombre, string $genero,string $multijugador):array | null {
  $stm = $this->conn->prepare("SELECT * from juegos where nombre LIKE :nombre AND genero LIKE :genero AND multijugador LIKE :multijugador  ORDER BY fechaAdicion DESC");
  $stm->execute(array(':nombre' => $nombre.'%' , ':genero' => $genero.'%',':multijugador' => $multijugador.'%'));
  $result = $stm->fetchAll();
  if ($result) {
    return $result;  
  }else{
      return null;
    } }
 

//query para poder hacer el filtro y select de generos
function getAllGenres(): array {
    $stm = $this->conn->query("SELECT * from generos");
    $stm->execute();
    return $stm->fetchAll();
  }
//query que usaremos para verificar usuario y contraseña cuando el usuario quiera autentificarse
function getadmin(string $username,  string $passwd): array  {
  $stm = $this->conn->prepare("SELECT * from admins where username = :username AND passwd= :passwd");
  $stm->execute(array(':username' => $username,':passwd' => $passwd));
  $result = $stm->fetch();
  if ($result) {
    return $result;
  }else{
    $result[0]= "el usuario y/o contraseña no corresponde con la base de datos" ;
  return $result;
  }
}
//query para obtener informacion de un juego en especifico
function getEx(string $nombre): Juego | null {
    $stm = $this->conn->prepare("SELECT * from juegos where nombre = :nombre");
    $stm->execute(array(':nombre' => $nombre));
    $result = $stm->fetch();
    if ($result) {
        return new Juego( $result["nombre"],  $result["desarrollador"], $result["genero"],  $result["multijugador"],
         $result["descripcionCorta"], $result["descripcionLarga"], $result["codigoVideoYoutube"],$result["codigoVideoYoutube2"],$result["fechaAdicion"]
    );
    } else {
        return null;
    }
}
//querys para obtener las imagenes de la base de datos
function getImage(String $nombre): String | null {
    $stm = $this->conn->prepare("SELECT imagen from juegos where nombre = :nombre");
    $stm->execute(array(':nombre' => $nombre));
    $result = $stm->fetch();
    if ($result) {
      return $result['imagen'];
    } else {
      return null;
    }
  }
  function getImage2(String $nombre): String | null {
    $stm = $this->conn->prepare("SELECT imagen2 from juegos where nombre = :nombre");
    $stm->execute(array(':nombre' => $nombre));
    $result = $stm->fetch();
    if ($result) {
      return $result['imagen2'];
    } else {
      return null;
    }
  }
  function getImage3(String $nombre): String | null {
    $stm = $this->conn->prepare("SELECT imagen3 from juegos where nombre = :nombre");
    $stm->execute(array(':nombre' => $nombre));
    $result = $stm->fetch();
    if ($result) {
      return $result['imagen3'];
    } else {
      return null;
    }
  }
  
//querys para actualizar la informacion de un juego, excepto la fecha, que nos interesa que siga igual
function update(Juego $juego): void {
   
    $stm = $this->conn->prepare("UPDATE juegos set  nombre=:nombre,desarrollador=:desarrollador, genero=:genero,  multijugador=:multijugador,descripcionCorta=:descripcionCorta, 
    descripcionLarga=:descripcionLarga,codigoVideoYoutube= :codigoVideoYoutube ,codigoVideoYoutube2=:codigoVideoYoutube2 where nombre=:nombre");
    $stm->execute(array( ":nombre" => $juego->nombre,":desarrollador" => $juego->desarrollador,  ":genero" => $juego->genero ,
    ":multijugador" => $juego->multijugador, ":descripcionCorta" => $juego->descripcionCorta, ":descripcionLarga" => $juego->descripcionLarga,
    ":codigoVideoYoutube" => $juego->codigoVideoYoutube,":codigoVideoYoutube2" => $juego->codigoVideoYoutube2
         ));
    }
    function updateimage (Juego $juego, $image){
      $stm = $this->conn->prepare("UPDATE juegos set imagen=:imagen where nombre=:nombre");
      $stm->execute(array(":nombre" => $juego->nombre, ':imagen' => $image));
    }
    function updateimage2 (Juego $juego, $image2){
      $stm = $this->conn->prepare("UPDATE juegos set imagen2=:imagen2 where nombre=:nombre");
      $stm->execute(array(":nombre" => $juego->nombre, ':imagen2' => $image2));
    }
    function updateimage3 (Juego $juego, $image3){
      $stm = $this->conn->prepare("UPDATE juegos set imagen3=:imagen3 where nombre=:nombre");
      $stm->execute(array(":nombre" => $juego->nombre, ':imagen3' => $image3));
    }

//query para meter nuevas entradas en la base de datos, la fecha de adicion se pondra automaticamente
function new(Juego $juego,$image,$image2,$image3): void {
    $stm = $this->conn->prepare("INSERT into juegos values (:nombre,  :desarrollador, :genero, :multijugador, :descripcionCorta, :descripcionLarga,
    :imagen,:imagen2, :imagen3,:codigoVideoYoutube,:codigoVideoYoutube2,:fechaAdicion)");
    $stm->execute(array(  ":nombre" => $juego->nombre, 
    ":desarrollador" => $juego->desarrollador,  ":genero" => $juego->genero , ":multijugador" => $juego->multijugador,
    ":descripcionCorta" => $juego->descripcionCorta , ":descripcionLarga" => $juego->descripcionLarga,':imagen' => $image,   ":imagen2" => $image2,":imagen3"=>$image3,
    ":codigoVideoYoutube" => $juego->codigoVideoYoutube,":codigoVideoYoutube2" => $juego->codigoVideoYoutube2,":fechaAdicion" => $juego->fechaAdicion
  ));
}
//query para borrar entradas en la base de datos
function delete( $juego): void {
    $stm = $this->conn->prepare("DELETE from juegos where nombre = :nombre" );
    $stm->execute(array(':nombre' => $juego));
}
}
