<?php session_start();
//si hay una session iniciada, la destruimos, para volver a entrar en el panel de administrador habra que autentificarse de nuevo
if (isset($_SESSION['username']) ){
session_destroy();
}
// nos  movemos a la pagina que ven los usuarios no administradores
header("Location: usuarios/index.php", true);
exit();
?>