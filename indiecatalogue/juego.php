<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
//el objeto juego y sus atributos
class Juego {
    function __construct (public String $nombre, public String  $desarrollador, public String  $genero, public String  $multijugador ,
    public String  $descripcionCorta,public String $descripcionLarga,public String $codigoVideoYoutube,public String $codigoVideoYoutube2,public String $fechaAdicion) {}
    function validate(): array {
        $errores =[];
        if (!isset($this->nombre) || strlen($this->nombre) <1) {
            $errores["juego"] = "el comentario debe estar asociado a un juego";
            
        }
        return $errores;
    }
}
