const { test,expect } = require('@playwright/test');

function delay(time) {
  return new Promise(function(resolve) { 
      setTimeout(resolve, time)
  });
}
test.beforeEach(async ({ page }) => {
  //login
  await page.goto('https://localhost/indiecatalogue/paneladmin/');
  await page.fill('input[name="usuario"]', 'admin');
  await page.fill('input[name="passwd"]', '1234');
  await page.click('text=enviar');
});

test('insertar_editar_y_borrar', async ({ page }) => {
//insertar
  await page.click('text=insertar');
  await page.fill('input[name="nombre"]', 'ccccc');
  await page.selectOption('select#genero', 'metroidvania');
  await page.selectOption('select#multijugador', 'online');
  await page.fill('input[name="desarrollador"]', 'bbbbbb');
  await page.fill('#descripcionCorta', 'aaaaaa');
  await page.fill('#descripcionLarga', 'dddddd');
  await page.fill('#codigoVideoYoutube', 'sdsdsdwet43');
  await page.fill('#codigoVideoYoutube2', 'fghf45546545');
  await page.setInputFiles('#image', 'imagenes/dex1.jpg');
  await page.setInputFiles('#image2', 'imagenes/dex2.jpg');
  await page.setInputFiles('#image3', 'imagenes/dex3.jpg');
  await page.click('text=guardar');
  //comprobamos que los datos se insertaron
  const locator1 = page.locator('.ccccc');
  const locator2 = page.locator('.bbbbbb');
  const locator3 = page.locator('.aaaaaa');
  const locator4 = page.locator('.dddddd');
  await expect(locator1).toContainText('ccccc');
  await expect(locator2).toContainText('bbbbbb');
  await expect(locator3).toContainText('aaaaaa');
  await expect(locator4).toContainText('dddddd');
  //editar
  await page.click('.aaaaaa1')
  await page.fill('#desarrollador', 'xxxxx');
  await page.selectOption('select#genero', 'metroidvania');
  await page.selectOption('select#multijugador', 'offline');
  //comprobamos que los datos se editaron
  await page.fill('#descripcionCorta', 'zzzzzzzzz');
  await page.fill('#descripcionLarga', 'nnnnnn');
  await page.fill('#codigoVideoYoutube', 'sdsdsdwet43');
  await page.fill('#codigoVideoYoutube2', 'fghf45546545');
  await page.click('text=guardar')
  const locator6 = page.locator('.xxxxx');
  const locator7 = page.locator('.zzzzzzzzz');
  const locator8 = page.locator('.nnnnnn');
  await expect(locator6).toContainText('xxxxx');
  await expect(locator7).toContainText('zzzzzzzzz');
  await expect(locator8).toContainText('nnnnnn');
//borrar
  await page.click('.nnnnnn1')
  await page.click('.denegar');
  await expect(locator6).toContainText('xxxxx');
  await expect(locator7).toContainText('zzzzzzzzz');
  await expect(locator8).toContainText('nnnnnn');
  await page.click('.nnnnnn1')
  await delay(500);
  await page.click('.confirmar');
  await expect(page).not.toHaveURL('https://localhost/indiecatalogue/paneladmin/borrar/deleteConfirm_controller.php?nombre=ccccc');
});



 


