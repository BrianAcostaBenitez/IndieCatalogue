const { test,expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  //login
  await page.goto('https://localhost/indiecatalogue/paneladmin/');
  await page.fill('input[name="usuario"]', 'admin');
  await page.fill('input[name="passwd"]', '1234');
  await page.click('text=enviar');


});

test('pagina_detallada', async ({ page }) => {
  //entramos a la pagina detallada y comprobamos que salen los datos, no podemos testear los precios porque estos cambian con el tiempo
  await page.click('text=dex');
  const locator1 = page.locator('#rating');
  await expect(locator1).toContainText('porcentaje');
  const locator2 = page.locator('.descripcion');
  await expect(locator2).toContainText('ciberneticos');
  //salimos de la pagina detallada y volvemos al panel de administrador
  await page.click('#volver');
  console.log(page.url());
  expect(page.url()).toContain("indiecatalogue/paneladmin/adminlistJuego_controller");
});
