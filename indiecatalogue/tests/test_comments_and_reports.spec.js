const { test,expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
    //login
    await page.goto('https://localhost/indiecatalogue/');
  });
  test('comentar', async ({ page }) => {
    await page.click('text=dex');
    //introducimos 2 comentarios y comprobamos que estan ahi
    await page.fill('input[name="alias"]', 'A12345B6789');
    await page.fill('#comentario', 'A12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12');
    await page.click('text=enviar');
    await page.fill('input[name="alias"]', 'C12345D6789');
    await page.fill('#comentario', 'C12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12');
    await page.click('text=enviar');
    const locator1 = page.locator('.A12345B6789alias');
    const locator2 = page.locator('.C12345D6789alias');
    await page.waitForTimeout(1000);
    await expect(locator1).toContainText('A12345B6789');
    await expect(locator2).toContainText('C12345D6789');
    //hacemos 2 reportes a un comentario y uno mas al otro
    await page.click('.A12345B6789reportar');
    await page.selectOption('select#motivo', 'insultos');
    await page.click('.ReportSend');
    await page.waitForTimeout(550);
    await page.click('.A12345B6789reportar');
    await page.selectOption('select#motivo', 'discriminacion');
    await page.click('.ReportSend');
    await page.waitForTimeout(550);
    await page.click('.C12345D6789reportar');
    await page.selectOption('select#motivo', 'difamacion');
    await page.click('.ReportSend');
    await page.waitForTimeout(500);
  });
  test('borrar reportes de un comentario y borrar otro', async ({ page }) => {
    //borraremos los reportes del pirmer comentario ,comprobando que el del segundo comentario sigue ahi
    await page.goto('https://localhost/indiecatalogue/paneladmin/');
    await page.fill('input[name="usuario"]', 'admin');
    await page.fill('input[name="passwd"]', '1234');
    await page.click('text=enviar');
    await page.click('#reportes');
    await page.click('.A12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12reporte');
    const locator3 = page.locator('.C12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12comprobacion');
    await expect(locator3).toContainText('C12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12');
    await page.click('.C12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12comentario');
    await page.waitForTimeout(500);
  });
  test('reportar de nuevo el primer comentario para asi borrarlo', async ({ page }) => {
    //volvemos a reportar el primer comentario para ahora borrarlo y que no moleste al realizar otra vez el test, de paso comprobamos
    //que sale el mensaje que indica que no hay reportes
    await page.goto('https://localhost/indiecatalogue/');
    await page.click('text=dex');
    await page.waitForTimeout(1500);
    await page.click('.A12345B6789reportar');
    await page.selectOption('select#motivo', 'insultos');
    await page.click('.ReportSend');
    await page.waitForTimeout(2500);
    await page.goto('https://localhost/indiecatalogue/paneladmin/');
    await page.fill('input[name="usuario"]', 'admin');
    await page.fill('input[name="passwd"]', '1234');
    await page.click('text=enviar');
    await page.click('#reportes');
    await page.click('.A12345B6789A12345B6789A12345B6789A12345B6789A12345B6789A12comentario');
    const locator4 = page.locator('#mensaje');
    await expect(locator4).toContainText('No hay ningun reporte, todo en orden');
  });



