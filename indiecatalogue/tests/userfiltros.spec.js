const { test,expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  //enter
  await page.goto('https://localhost/indiecatalogue/usuarios');
});
//todos los filtros a la vez
test('todos juntos', async ({ page }) => {
    await page.fill('input[name="nombre"]', 'undertale');
    await page.selectOption('select#genero', 'Rol');
    await page.selectOption('select#multijugador', 'no');
    await page.click('#submit');
    const locator = page.locator('.undertale');
    await expect(locator).toContainText('undertale');
    //restablecemos filtros
    await page.click('#restablecerFiltros');
    const locator2 = page.locator('.Dex');
    await expect(locator).toContainText('undertale');
    await expect(locator2).toContainText('Dex');
  });
  //testeando los filtros en solitario
  test('uno por uno', async ({ page }) => {
    // por nombre
    await page.fill('input[name="nombre"]', 'undertale');
    await page.click('#submit');
    const locator = page.locator('.undertale');
    await expect(locator).toContainText('undertale');
    //por genero
    await page.selectOption('select#genero', 'Rol');
    await page.click('#submit');
    await expect(locator).toContainText('undertale');
    //por multijugador
    await page.selectOption('select#multijugador', 'no');
    await page.click('#submit');
    await expect(locator).toContainText('undertale');
   
  });
  //testeando los filtros en combinaciones de dos
  test('combinaciones', async ({ page }) => {
    // por nombre y genero
    await page.fill('input[name="nombre"]', 'undertale');
    await page.selectOption('select#genero', 'Rol');
    await page.click('#submit');
    const locator = page.locator('.undertale');
    await expect(locator).toContainText('undertale');
    //por genero y multijugador
    await page.selectOption('select#genero', 'Rol');
    await page.selectOption('select#multijugador', 'no');
    await page.click('#submit');
    await expect(locator).toContainText('undertale');
    //por multijugador y nombre
    await page.selectOption('select#multijugador', 'no');
    await page.fill('input[name="nombre"]', 'undertale');
    await page.click('#submit');
    await expect(locator).toContainText('undertale');
  });
  test('sin resultados', async ({ page }) => {
    // comprobamos que al no haber resultados salga el mensaje que lo indica
    await page.fill('input[name="nombre"]', 'pruebaParaQueNoSalganResultados');
    await page.click('#submit');
    const locator = page.locator('#mensaje');
    await expect(locator).toContainText('No se encontraron resultados');
    });