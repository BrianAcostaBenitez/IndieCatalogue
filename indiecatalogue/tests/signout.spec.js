const { test,expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  //login
  await page.goto('https://localhost/indiecatalogue/paneladmin/');
  await page.fill('input[name="usuario"]', 'admin');
  await page.fill('input[name="passwd"]', '1234');
  await page.click('text=enviar');
});
//salimos del panel de administrador y comprobamos que llegamos a la pagina de usuarios
test('salir del panel de administrador', async ({ page }) => {
    await page.click('#signout');
    console.log(page.url());
    expect(page.url()).toContain("indiecatalogue/usuarios/");
  });