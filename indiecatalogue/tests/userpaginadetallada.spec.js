const { test,expect } = require('@playwright/test');

test.beforeEach(async ({ page }) => {
  //entramos sin autentificarnos
  await page.goto('https://localhost/indiecatalogue/usuarios');
});

test('pagina_detallada', async ({ page }) => {
  //entramos a una pagina detallada y vemos que estan las cosas como deberian
  await page.click('text=dex');
  const locator1 = page.locator('#rating');
  await expect(locator1).toContainText('porcentaje');
  const locator2 = page.locator('.descripcion');
  await expect(locator2).toContainText('ciberneticos');
  //salimos de la pagina detallada y volvemos a la pagina principal de usuarios
  await page.click('#volver');
  console.log(page.url());
  expect(page.url()).toContain("indiecatalogue/usuarios/index");
});
