<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../factoryConnection.php";
require_once "../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection;
$config = require_once "../config.php";

try {
  
  $factory = new FactoryConnection($config);
  $repository = new PDOJuegoRepository($factory->get());
  //llamamos a las querys que dan todos los datos de la tabla de juegos, sacaremos informacion de ahi para la pagina principal 
  $juegos=$repository->getAll();
  //tambien llamamos la query que da la informacion de la tabla de generos para el filtro de los generos
  $generos = $repository->getAllGenres();
  //la pagina principal
  require "paginaprincipal.php";
  //si no hay entradas en la tabla, saldra un mensaje indicandolo
  if (is_null($juegos)){
    ?>
    <script>
          document.getElementById("mensaje").innerHTML ='No hay entradas en la base de datos ';
    </script>
    <?php
  }  
  //si esto falla, saldra un mensaje de error
} catch (PDOException $e) {
  print "¡Error!:" . $e->getMessage() . "<br/>";
  die();
} finally {
    $repository = null;
}
