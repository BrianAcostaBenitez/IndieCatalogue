<!doctype html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" >
<meta name="viewport" content ="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="index.css">
<title>indiecatalogue</title>
</head>
<!-- pagina principal para los usuarios-->
<body>

<div class="row justify-content-around">
    <div class="col-lg-10 col-12 container1 ">
        <div class="texto1"><h2>indie catalogue</h2></div>
        <div class="texto2"><h2>un catalogo de indies de calidad para elegir</h2></div>
        <div class="texto3"><p>¿quieres un juego indie pero no sabes decidirte con tanto juego por ahi? 
        pues aqui estamos, con esta lista te sera mas facil asegurarte de que tu decision no sera equivocada</p></div> </div>
    </div>
<!-- aqui tenemos los filtros, tenemos un input y dos select, en orden ,de nombre ,genero(opciones sacadas de la base de datos),
 y de capacidad multijugador-->
<form action="../funciones_comunes/filtro_controller.php"  method="post" enctype="multipart/form-data">
<div class ="filtrobusqueda">
        <div class="row">
            <div class="container2 col-lg-3 col-md-2 col-2  offset-1 " >
                <label for="">buscar por</label>
            </div>
            <div class="container2  col-lg-2 col-md-3 col-7 ">
        <!-- filtrar por nombre, por ejemplo si ponemos "re" saldran los juegos cuyo nombre empieze por "re"-->
                <label for="nombre"> nombre</label>
                <input type="text" name=nombre  id='nombre'>
            </div>

    <div class="container2 col-lg-2 col-md-3 col-7 offset-1">
        <!-- filtrar por genero, saldran los juegos del genero que especifiquemos-->
        <label for="genero">genero</label>
        <select name=genero id="genero" class="genero">
            <option value="cualquiera">cualquiera</option>
                <?php foreach($generos as $genero): ?>
                    <option value="<?=$genero['genero']?>">
                    <?= $genero['genero']?>
                <?php endforeach;?>
            </option>
        </select>
    </div>

    <div class="container2  col-lg-2 col-md-3 col-7 offset-1">
        <!-- filtrar por multijugador, saldran los juegos que tengan la capacidad de multijugador que desemos-->
        <label for="multijugador">multijugador</label>
            <select name=multijugador id="multijugador">
                <option value="cualquiera">cualquiera</option>
                <option value="no">no</option>
                <option value="offline">offline</option>
                <option value="online">online</option>
                <option value="ambos">ambos</option>
            </select>
    </div>
    <!-- boton submit,al hacer click aqui se aplicaran los filtros-->
    <input class="col-lg-2 col-md-3 col-7 offset-1 container2 button" id='submit' name='submit' type= "submit" value="buscar">
    </div>
    </div>

</form>
<div class="row justify-content-around">
    <div class ="tabla col-lg-10 col-12 container2 ">
    <!--restablecer los filtros-->
    <a class='button tableButton' id="restablecerFiltros" href="../usuarios"><?='restablecer filtros'?> </a>
    <!-- esta es la tabla donde el usuario vera la informacion, al hacer click en el nombre o en la imagen ,entraran en la pagina detallada
    tambien hay barra de desplazamiento vertical,-->
        <div style="overflow:scroll;height:500px; " class="col-12">
            <div  class ="col-12  container2"  id='mensaje'>

                <table>
                    <tr>
                        <th>imagen</th><th>nombre</th><th>desarrollador</th><th>genero </th><th>multijugador </th><th>descripcion corta</th>
                    </tr>
                        <?php foreach ($juegos as $juego ):?>
                            <tr>
                                <td class="col-2"><a href="../funciones_comunes/pagina_detallada/detallada_controller.php?nombre=<?=urlencode($juego['nombre'])?>"><img src="../getimages/getimage.php?nombre=<?=urlencode($juego['nombre'])?>" 
                                            width="90px" height="90px">
                                <td class="<?=$juego["nombre"]?>  col-2"><a href="../funciones_comunes/pagina_detallada/detallada_controller.php?nombre=<?=urlencode($juego['nombre'])?>">   <?=$juego["nombre"] ?></td>
                                <td class="col-2"><?=$juego["desarrollador"] ?> </td>
                                <td class="<?=$juego["genero"] ?>col-1"><?=$juego["genero"] ?></td>
                                <td class="col-1"><?=$juego["multijugador"] ?> </td>
                                <td class="col-4"><?=$juego["descripcionCorta"] ?> </td>   
                            </tr>
                        <?php endforeach; ?>
                </table>
            </div>
        </div>
            <!--licencia creative commons "Reconocimiento-NoComercial-SinObraDerivada" -->
        <img src="../imagenes/creativecommons.png" alt='licencia creative commons "Reconocimiento-NoComercial-SinObraDerivada" ' > 
    </div>
</div>
</div>
</body>


</html>