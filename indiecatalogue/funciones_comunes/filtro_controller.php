<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../factoryConnection.php";
require_once "../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../config.php";
try {
  
  $factory = new FactoryConnection($config);
  $repository = new PDOJuegoRepository($factory->get());
  //cogemos los datos del formularios de filtros
  $nombre=$_POST["nombre"];
  $genero=$_POST['genero'];
  $genero= strval($genero);
  $multijugador=$_POST['multijugador'];
  $multijugador= strval( $multijugador);
  //si no hemos puesto nada o dejado el valor por defecto, entonces el valor sera %, 
  //lo cual nos servira para hacer que la busqueda no tenga el parametro en cuenta
  if ($nombre === ''){
    $nombre = "%";
  }
  if ($multijugador=== 'cualquiera'){
    $multijugador = "%";
  }
  if ($genero=== 'cualquiera'){
    $genero = "%";
  }
  if (($nombre === '') &&($multijugador==='cualquiera')&& ( $genero==='cualquiera')) {
    $juegos=$repository->getAll();
     }
  //seguimos necesitando sacar la lista de generos
  $generos = $repository->getAllGenres();
  //se realiza la query que hace la busqueda
  $juegos=$repository-> getbyallfilters($nombre,$genero,$multijugador);
  //si el usuario es administrador, se nos mostrala el panel de administrador con el resultado de la busqueda
 
  session_start(); 
    if (isset($_SESSION['username']) ){
        require "../paneladmin/paneladmin.php";
    // si no lo es, se hara en la pagina principal
    }else {
        require "../usuarios/paginaprincipal.php"; 
        }
    // si no se encuentran resultados, entonces aparecera un mensaje que nos lo indicara
    if (is_null($juegos)){
      ?>
      <script>
            document.getElementById("mensaje").innerHTML ='No se encontraron resultados ';
      </script>
      <?php
  }  
} catch (PDOException $e) {
print "¡Error!:" . $e->getMessage() . "<br/>";
die();
} finally {
    $repository = null;

}
