<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
//el objeto reporte y sus atributos
class Reporte {
    function __construct (public int $id_reporte, public int  $id_comentario, public String  $motivo) {}
    function validate(): array {
        $errores =[];
        if (!isset($this->id_comentario)){
            $errores["city"] = "el reporte debe estar asociado a un id_comentario";            
        }
        return $errores;
    }
}
