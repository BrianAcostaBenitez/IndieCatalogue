<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "reporte.php";
require_once "../../factoryConnection.php";
require_once "pdoReportesRepository.php";
require_once "../../pdoJuegoRepository.php";

use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
    $config = require_once "../../config.php";
    $factory = new FactoryConnection($config);
    $repository = new PDOReportesRepository($factory->get());
    $juegosrepository = new PDOJuegoRepository ($factory->get());
    //conseguimos la id del reporte y sacamos el juego del cual hemos cogido el reporte
    $juego = $juegosrepository->getEx($_POST["namegame"]);
    header ("location:../pagina_detallada/detallada_controller.php?nombre=$juego->nombre");
    $id_report = $repository->getNextNumber();$nameGame = $juego->nombre;
    //rellenamos los atributos del objeto reporte
    $reporte = new Reporte($id_report,intval($_POST["id_comentario"]),$_POST["motivo"]);
    $repository->new($reporte);
    //volvemos a la pagina del juego donde esta el comentario que hemos reportado
  
