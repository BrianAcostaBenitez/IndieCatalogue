<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
require_once "reporte.php";
class PDOReportesRepository {
    function __construct(\PDO $conn) {
      $this->conn = $conn;
}
//obtenemos la id mas alta de los reportes para luego sumarle 1,el resultado sera la id de las nuevas entradas, de ese modo no hay que ponerle id manualmente
function getNextNumber(): int {
    $stm = $this->conn->query("select max(id_reporte) as maxNumber from reportes;");
    $stm->execute();
    $result=$stm->fetch()['maxNumber'];
    print_r ($result);
    if ($result){
      return intval($result+1);
      //si no hay ninguno entonces la id del primero sera 1
    }else{
            $result=1;
    return $result;
}}
//obtener todos los reportes
function getAll(): array | null {
    $stm = $this->conn->prepare("SELECT * from reportes");
      $stm->execute();
      $result = $stm->fetchAll();
      if ($result) {
        return $result;
    } else {
      return null; 
    } }

//obtener los comentarios reportados, ordenados por id del reporte
function getReportedComments(): array | null {
      $stm = $this->conn->prepare("SELECT comentarios.id,comentarios.comentario, reportes.id_reporte,reportes.motivo
      FROM comentarios
      LEFT JOIN reportes
      ON comentarios.id= reportes.id_comentario
      WHERE comentarios.id= reportes.id_comentario
      ORDER BY reportes.id_comentario;");
        $stm->execute();
        $result = $stm->fetchAll();
        if ($result) {
          return $result;
      } else {
        return null; 
      } }

//query para obtener informacion de un reporte especifico
function getEx(int $id): reporte | null {
    $stm = $this->conn->prepare("SELECT * from reportes where id = :id");
    $stm->execute(array(':id' => $id));
    $result = $stm->fetch();
    if ($result) {
        return new reporte( $result["id_reporte"],$result["id_comentario"],$result["motivo"]
    );
    } else {
        $result[0]= "no-result";
      return $result;
    }
}

//query para meter nuevos reportes en la base de datos
function new(reporte  $reporte ): void {
    $stm = $this->conn->prepare("INSERT into reportes values (:id_reporte,:id_comentario,:motivo)");
    $stm->execute(array(  ":id_reporte" => $reporte->id_reporte,  ":id_comentario" => $reporte->id_comentario,  ":motivo" => $reporte->motivo
  ));
}

//query para borrar reportes en la base de datos
function deleteReports($id): void {
    $stm = $this->conn->prepare("DELETE from reportes where id_comentario = :id" );
    $stm->execute(array(':id' => $id));
}
}
