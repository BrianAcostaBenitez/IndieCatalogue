<!DOCTYPE html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content ="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
<link rel="stylesheet" href="reporte.css">
<style>
      
</style>
<title>crear reporte</title>
</head>
<body>

<div class="row justify-content-around">
    <div class="col-lg-10 col-md-10 col-12 container1 ">
        <div class="row justify-content-around">
            <div class="col-lg-10 col-md-10 col-12 container2" id='mensaje'>
                <p>comentario a reportar: </p>
                <!--aqui mostramos el comentario que vamos a reportar-->
                <p><?=$comentario->comentario ?></p>
                <p>perteneciente a la pagina: <?=$comentario->juego ?>
            </div>
        </div>
        
            <div class="row justify-content-around">
            <div class=" col-lg-10 col-md-10 col-12 container2">
            <form action="addReport_controller.php"  method="post" enctype="multipart/form-data">
                <input type="text" id="namegame" name="namegame" value="<?=$comentario->juego?>" hidden>
                <input type="text" id="id_comentario" name="id_comentario" value="<?=$comentario->id?>" hidden>
                <div class="row">
                    <!--aqui el usuario selecciona el motivo del reporte-->
                    <div class="container2 col-lg-10 col-md-10 col-12 ">
                        <label for="motivo">por favor, especfique el motivo por el cual quiere reportar el comentario, un administrador lo consultara </label>
                            <select name="motivo" id="motivo" >
                                <option value="insultos">insultos</option>
                                    <option value="discriminacion">discriminacion</option>
                                    <option value="difamacion">difamacion</option>
                                    <option value="incitacion al odio/violencia">incitacion al odio/violencia</option>
                                    <option value="lenguaje excesivamente soez">lenguaje excesivamente soez</option>
                            </select>
                    </div>
                </div>
            </div>
                            <!--aqui decidimos si enviamos el reporte o no-->
                            <input class="ReportSend container2  col-lg-5 col-md-5 col-7 " type= "submit" value="enviar reporte">
                            <a class='container2 button  col-lg-5 col-md-5 col-7'  href="../pagina_detallada/detallada_controller.php?nombre=<?=urlencode($comentario->juego)?>">no reportar</a>
    
            </form>
    </div>
</div>
</div>
</body>
</html>