<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../factoryConnection.php";
require_once "../comentarios/pdoComentariosRepository.php";

use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
//sacamos la id del comentario que queremos reportar junto con el comentario en si mismo y cargamos la pagina para crear el reporte
try {
    $config = require_once "../../config.php";
    $factory = new FactoryConnection($config);
    $repository =  new PDOComentariosRepository($factory->get());
    $id_comentario = $_GET["id_comentario"];
    $id = intval($id_comentario);
    $comentario = $repository->getEx($id);
     
    require "crear_reporte.php";
}
catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $repository = null;
}
