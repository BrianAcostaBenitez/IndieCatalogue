<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
//el objeto juego y sus atributos
class Comentario {
    function __construct (public int $id, public String  $juego, public String  $alias, public String  $comentario, public string $fecha, public int $userType) {}
    function validate(): array {
        $errores =[];
        if (!isset($this->nombre) || strlen($this->nombre) <1) {
            $errores["nombre"] = "el comentario debe estar asociado a un juego";
            
        }
        return $errores;
    }
}
