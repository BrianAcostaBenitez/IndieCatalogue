<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
require_once "comentarios.php";
class PDOComentariosRepository {
    function __construct(\PDO $conn) {
      $this->conn = $conn;
}
//obtener la id mas alta de los comentarios para luego sumarle 1, asi obtenemos la nueva id y no hay que ponerla manualmente
function getNextNumber(): int {
    $stm = $this->conn->query("SELECT max(id) as maxNumber from comentarios;   ");
    $stm->execute();
    $result=$stm->fetch()['maxNumber'];
    print_r ($result);
    if ($result){
      return intval($result+1);
    }else{
            $result=1;
    return $result;
}}
//obtener todos los comentarios
function getAll(): array | null {
    $stm = $this->conn->prepare("SELECT * from comentarios ORDER BY id DESC");
      $stm->execute();
      $result = $stm->fetchAll();
      if ($result) {
        return $result;
    } else {
      return null; 
    } }

//query para obtener informacion de los comentarios de un juego ordenados por id de comentario, veremos los mas recientes mas arriba
function getAllCommentsOfGame(string $nombre): array | null {
  $stm = $this->conn->prepare("SELECT * from comentarios where juego = :nombre ORDER BY id DESC");
  $stm->execute(array(':nombre' => $nombre));
  $result = $stm->fetchAll();
  return $result;
  if ($result) {
    return $result;
} else {
  return null; 
} }

//query para obtener informacion de un comentario especifico
function getEx(int $id): Comentario | null {
    $stm = $this->conn->prepare("SELECT * from comentarios where id = :id");
    $stm->execute(array(':id' => $id));
    $result = $stm->fetch();
    if ($result) {
        return new Comentario( intval($result["id"]),  $result["juego"], $result["alias"],  $result["comentario"], $result["fecha"], intval($result["userType"])
    );
    } else {
      return null; 
    }
}

//query para meter nuevos comentarios en la base de datos
function new(Comentario $comentario): void {
    $stm = $this->conn->prepare("INSERT into comentarios values (:id,  :juego, :alias, :comentario, :fecha, :userType)");
    $stm->execute(array(  ":id" => $comentario->id, 
    ":juego" => $comentario->juego,  ":alias" => $comentario->alias, ":comentario" => $comentario->comentario,
    ":fecha" => $comentario->fecha,":userType" => $comentario->userType
  ));
}
//query para borrar comentarios en la base de datos
function deleteComment( $id): void {
    $stm = $this->conn->prepare("DELETE from comentarios where id = :id" );
    $stm->execute(array(':id' => $id));
}
}
