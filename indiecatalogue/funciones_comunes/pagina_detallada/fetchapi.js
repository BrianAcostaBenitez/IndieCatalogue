//aqui hacemos las llamadas a la api
async function getapi(urlname) {
    // empezamos haciendo tres llamadas, en primer lugar recogemos los datos que sacamos con el link que hemos usado 
    // para sacar el "game id" de un juego
    const responsename = await fetch(urlname);
    let namedata = await responsename.json();
    //la api lo que hace es hacer una busqueda cuyo resultado es todos los juegos que empiezan por el nombre que hemos puesto, al poner el nombre completo,
    //el juego que queremos sera el primer resultado (o mas probablemente el unico) de la lista
    namedata.list=namedata;
    gameid= namedata[0].gameID;
   //despues usamos ese game id en otra llamada, de aqui sacamos la informacion de los precios
    const url = "https://www.cheapshark.com/api/1.0/games?id="+gameid;
    const response = await fetch(url);
    let data = await response.json();
    //hora sacamos la informacion de las tiendas,ya que necesitamos los nombres de las tiendas
    urlstores="https://www.cheapshark.com/api/1.0/stores"
    const responsestores = await fetch(urlstores);
    let datastores = await responsestores.json();
    //si la llamada tiene exito sacaremos los datos con la funcion show
    if (response) {
        show(data,datastores);
    }
    //si no, nos saldra un mensaje diciendonoslo
   else{
    document.getElementById("info").innerHTML = "no se encontraron datos, esto puede deberse a un error o que la api de cheapshark no puede proporcionar datos en este momento, por favor, espere a que se solucione";
   }
}
    //de esta funcion (que la llamamos mas abajo)sacamos la informacion sobre la puntuacion de las reseñas
async function getratings(dealID) {
    //hacemos la llamada
    const urldeals= "https://www.cheapshark.com/api/1.0/deals?id="+dealID;
    const responsedeal = await fetch(urldeals);
    let ratinginfo = await responsedeal.json();
    ratinginfo.list=ratinginfo;
    let rating;
    //y ponemos la informacion en la variable rating
    rating = `
    <p> <span style="font-weight: bold;">porcentaje de reseñas positivas en steam:</span> ${ratinginfo.gameInfo.steamRatingPercent}%</p>
    <p> <span style="font-weight: bold;"> puntuacion promedio metacritic:</span> ${ratinginfo.gameInfo.metacriticScore} </p>
    <p>  <a href="https://www.metacritic.com/${ratinginfo.gameInfo.metacriticLink}">Leer reseñas de Metacritic</a>  </p>
    `;
    //la informacion se muestra en el div con el id "rating"
    document.getElementById("rating").innerHTML = rating;
}

// mostramos los datos
function show(data,datastores) {
    //preparamos los datos para hacer el bucle for
    data.list=data;
    //hacemos lo mismo con los datos de las tiendas
    datastores.list=datastores;
    //declaramos las variables que usaremos para mostrar la lista de tiendas y precios
    let precios;
    let storeID;
    //sacamos un deal id de data.list ,cualquiera nos vale,pues la informacion que sacaremos sera la misma ,luego llamamos a la funcion getratings
    dealID= data.deals[0].dealID;
    getratings(dealID);

    // hacemos el bucle for para sacar todos los precios que hay actualmente
 for(let deal of data.deals) {
     storeID=deal.storeID;
     nameofstore=datastores[storeID-1].storeName;
    //ponemos la informacion en la variable precios
   precios+= `
<p><span><a href="https://www.cheapshark.com/redirect?dealID=${deal.dealID}">${nameofstore} </a></span>${deal.price} €</p>`;
}
    // y la ponemos en el div con la id precios
    document.getElementById("precios").innerHTML = precios;
}