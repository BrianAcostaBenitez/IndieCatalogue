<?php
//si hay una sesion iniciada, volvemos al panel de administrador, si no, volveremos a la pagina principal de usuarios
session_start();
if (isset($_SESSION['username']) ){ 
    header("Location: ../../paneladmin/adminlistJuego_controller.php", true);
}else{
    header("Location: ../../usuarios/index.php", true);
}