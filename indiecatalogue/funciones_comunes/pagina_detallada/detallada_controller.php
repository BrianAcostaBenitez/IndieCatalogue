<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";
require_once "../comentarios/pdoComentariosRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../../config.php";
try {
  //sacamos la informacion de un juego especifico y sus comentarios y luego cargamos la pagina detallada

  $factory = new FactoryConnection($config);
  $repository = new PDOJuegoRepository($factory->get());
  $juego=$repository->getEX($_GET["nombre"]);
  $comentariosrepository = new PDOComentariosRepository($factory->get());
  $comentarios=$comentariosrepository->getAllCommentsOfGame($_GET["nombre"]);
  require "detallada.php";
  //si no hay nignun comentario entonces saldra un mensaje que se nos indicara
  if (is_null($comentario)){
    ?>
    <script>
          document.getElementById("mensaje").innerHTML ='No hay comentarios, se el primero en poner tu comentario ';
    </script>
    <?php
  } 

} catch (PDOException $e) {
print "¡Error!:" . $e->getMessage() . "<br/>";
die();
} finally {
    $repository = null;

}
