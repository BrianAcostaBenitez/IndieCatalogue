<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,	initial-scale=1, shrink-to-fit=no,user-scalable=no">
<link rel="stylesheet"  href="../../css/bootstrap.min.css">
<link rel="stylesheet"  href="detallada.css">
<script src="fetchapi.js"></script> 
<!-- en esta pagina se muestran los datos y precios actuales del juego -->
<title>indiecatalogue <?=$juego->nombre?></title>
<script>
   //llamamos la funcion que hace las llamadas a la api tras añadir el nombre del juego a la url
  const urlname = "https://www.cheapshark.com/api/1.0/games?title=<?=$juego->nombre?>";
  getapi(urlname)
</script>
</head>
<body>
<?php

?>
<!-- boton para que un controlador nos lleve a la vista de la seccion de donde hemos venido-->
  <button class="float-left button container2" id="volver" onClick="location.href='volver_a_vista_controller.php'">volver</button>


    <div class="row col-lg-12  container2 margenes" style="position:relative; top:50px;" >
<!-- usamos los controladores de la carpeta get/images para sacar las imagenes -->
  <div class= "col-lg-3 col-md-12 col-12 imagenes ">
    <img src="../../getimages/getimage.php?nombre=<?=urlencode($juego->nombre)?>"  class=" w-100 image1"  height=33%;>   
    <img src="../../getimages/getimage2.php?nombre=<?=urlencode($juego->nombre)?>" class=" w-100 image2" height=34%;> 
    <img src="../../getimages/getimage3.php?nombre=<?=urlencode($juego->nombre)?>"  class="w-100 image3" height=33%;>
  </div>
  <div class="col-lg-7 col-md-8 col-8">
    <!-- mostramos los datos -->
    <p>nombre:<?=$juego->nombre?></p> 
    <p>desarrollador:<?=$juego->desarrollador?></p> 
    <p> genero:<?=$juego->genero ?></p>
    <p> multijugador:<?=$juego->multijugador?></p>
  <br>
    <div class="descripcion">
      <p><?=$juego->descripcionLarga ?></p>
      <div class="row">
      <iframe class="col-lg-6 col-12" width="330" height="300" src="https://www.youtube.com/embed/<?=$juego->codigoVideoYoutube?>"frameborder="0" allowfullscreen></iframe>
      <iframe class="col-lg-6 col-12" width="330" height="300" src="https://www.youtube.com/embed/<?=$juego->codigoVideoYoutube2?>"frameborder="0" allowfullscreen></iframe>
    </div>
    </div>
  </div>
  <!-- mostramos los datos que nos dara la api, a partir del nombre sacamos la puntuacion promedio de reseñas, 
 junto con un enlace a metacritic para poder ir a leerlas personalmente y tambien los precios, al clickear el nombre de la tienda nos
 llevara a la tienda-->
  <div class=" col-lg-2 col-md-4 col-4 precios">  
    <div id="rating"></div>
  <p>precios</p>
    <div id="precios"></div>
    </div>
    <div class="row container2 ">
    <div class="col-12  comentarios  container2">
         <!--formulario de comentarios-->
    <form action="../comentarios/addcomentario_controller.php"  method="post" enctype="multipart/form-data">
    <div class="container2 offset-1 row">
        
    <div class=" col-lg-4 col-md-4 col-4">
        <!--este campo oculto nos asegura que los comentarios que se hacen en una paginad detallada se quedan en la pagina donde
        pusimos el comentario, es decir, el comentario solo saldra en la pagina del juego donde lo pusimos-->
    <input type="text" id="juego" name="juego" value="<?=$juego->nombre?>" hidden >
        <!--campo de alias con el queremos que se nos identifique-->
                <label for="alias"> alias</label>
                <input type="text" id="alias" name="alias" required>
        </div>
         <!--en este textarea escribimos nuestro comentario-->
        <div class="col-lg-9 col-md-9 col-9 ">
                <label for="comentario">comentario</label>
                <textarea rows="4" cols="120" style="max-width:100%;"  name="comentario" id="comentario"  maxlength="1500"></textarea>
        </div>
          <!--con el input submit lo enviamos-->
        <input type= "submit" class=" col-12 float-left button container2"  value="enviar comentario">
    </div>
    </form>
    </div>
    <!--aqui tenemos la tabla de comentarios, si no hay comentarios, aparecera un mensaje que nos lo indicara-->
    <div class="col-10 offset-1 " id="mensaje">
    <!--decidimos el tamaño horizontal de la tabla y de cada columna-->
    <table class="offset-1 tablaComentarios" style="width:85%">
<col style="width:5%">
<col style="width:10%">
<col style="width:45%">
<col style="width:15%">
<col style="width:10%">
      <?php  $usertype;
      foreach ($comentarios as $comentario ):  ?>
        <tr>
        <!--si el usuario que puso el comentario era un administrador, se indicara como tal-->
        <?php if ($comentario["userType"]== 1){
          $usertype="(admin)";
        }else{
          $usertype="";
        }
        ?>
        <!--aqui estan las filas-->
          <td ><?=$usertype?></td>
          <td class="<?=$comentario["alias"]."alias"?>" id="tdBordes"><?=$comentario["alias"].":"?></td>
          <td id="tdBordes"><?=$comentario["comentario"]?></td>
          <td id="tdBordes"><?=$comentario["fecha"] ?></td>
          <!--si un comentario es ofensivo, podemos reportarlo, los comentarios de administradores no se pueden reportar-->
          <?php if ($comentario["userType"]== 0){ ?>
          <td class="<?=$comentario["alias"]."reportar"?>" id="tdBordes"><a class='button report'  href="../reportes/prepare_report.php?id_comentario=<?=urlencode($comentario['id'])?>">reportar</a></td>
          <?php }?>
        </tr>
      <?php endforeach;?>
    </table>
    </div>
    
  </div>
  </div>
</body>
</html>