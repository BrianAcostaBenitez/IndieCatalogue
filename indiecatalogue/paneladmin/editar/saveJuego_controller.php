<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../juego.php";
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";

//rellenamos los atributos del objeto juego con los datos del formulario,menos la fecha, porque queremos que siga siendo la misma
$juego = new Juego($_POST["nombre"], $_POST["desarrollador"],$_POST["genero"],$_POST["multijugador"],
$_POST["descripcionCorta"] , $_POST["descripcionLarga"] ,$_POST["codigoVideoYoutube"],$_POST["codigoVideoYoutube2"] ,''
);
$errores = $juego->validate();
if (count($errores) > 0) {
    require "forminsertar.php";
die();
}
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
session_start();
if (isset($_SESSION['username']) ){
    $config = require_once "../../config.php";
        $factory = new FactoryConnection($config);
        $repository = new PDOJuegoRepository($factory->get());
        //las imagenes las recogemos aqui,si hemos seleccionado imagenes nuevas, se cambiaran, si no se mantendran las que estaban
    if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
        $image = file_get_contents($_FILES['image']['tmp_name']);
        $repository-> updateimage($juego,$image);
    }
    if (isset($_FILES['image2']) && $_FILES['image2']['size'] > 0) {
        $image2 = file_get_contents($_FILES['image2']['tmp_name']);
        $repository-> updateimage2($juego,$image2);
    }
    if (isset($_FILES['image3']) && $_FILES['image3']['size'] > 0) {
        $image3 = file_get_contents($_FILES['image3']['tmp_name']);
        $repository-> updateimage3($juego,$image3); 
    }
    //actualizamos la entrada
    $repository->update($juego);

    
header ("location:../adminlistJuego_controller.php");
}
else { 
    header ("Location: ../index.php");
}