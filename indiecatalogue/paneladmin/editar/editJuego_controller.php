<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
session_start();
require_once "../../juego.php";
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;

if (isset($_SESSION['username']) ){
try {
    $config = require_once "../../config.php";
    $factory = new FactoryConnection($config);
    $repository =  new PDOJuegoRepository($factory->get());
    //sacamos los datos actuales, seran los datos "por defecto" en el formulario
    $juego = $repository->getEx($_GET["nombre"]);
    //necesitaremos sacar la lista de generos para un select
    $generos = $repository->getAllGenres();
    require "formeditar.php";
}
catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $repository = null;
}
}else { 
  header ("Location: ../index.php");
} 