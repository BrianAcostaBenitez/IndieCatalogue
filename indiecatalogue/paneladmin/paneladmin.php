<!doctype html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content ="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="paneladmin.css">

<title>indiecatalogue panel admin</title>

</head>
<!-- pagina principal para los usuarios-->
<body>
<?php

        if (isset($_SESSION['username']) ){
?>
<div class="row justify-content-around">
    <div class="col-lg-10 col-12 container1 ">
    <div class="texto1"><p>bienvenido</p></div>
    <div class="texto2" ><p>aqui es posible editar la tabla de juegos de la base de datos,  para editar la de los generos y los usuarios que 
        pueden entrar al panel de administracion debera ir a la base de datos
    </p>
    <a class='button' id='signout' href="../"> <?='salir del panel de administrador'?></a></div>
    </div> 
</div>
<!-- aqui tenemos los filtros, tenemos un input y dos select, en orden ,de nombre ,genero(opciones sacadas de la base de datos),
 y de capacidad multijugador-->
<form action="../funciones_comunes/filtro_controller.php"  method="post" enctype="multipart/form-data">
    <div class ="filtrobusqueda">
        <div class="row">
            <div class="container2 col-lg-3 col-md-2 col-2  offset-1 " >
                <label for="">buscar por</label>
            </div>
            <div class="container2  col-lg-2 col-md-3 col-7 ">
        <!-- filtrar por nombre, por ejemplo si ponemos "re" saldran los juegos cuyo nombre empieze por "re"-->
                <label for="nombre"> nombre</label>
                <input type="text" name=nombre  id='nombre'>
            </div>

    <div class="container2 col-lg-2 col-md-3 col-7 offset-1">
        <!-- filtrar por genero, saldran los juegos del genero que especifiquemos-->
        <label for="genero">genero</label>
        <select name=genero id="genero" class="genero">
            <option value="cualquiera">cualquiera</option>
                <?php foreach($generos as $genero): ?>
                    <option value="<?=$genero['genero']?>">
                    <?= $genero['genero']?>
                <?php endforeach;?>
            </option>
        </select>
    </div>

    <div class="container2  col-lg-2 col-md-3 col-7 offset-1">
        <!-- filtrar por multijugador, saldran los juegos que tengan la capacidad de multijugador que desemos-->
        <label for="multijugador">multijugador</label>
            <select name=multijugador id="multijugador">
                <option value="cualquiera">cualquiera</option>
                <option value="no">no</option>
                <option value="offline">offline</option>
                <option value="online">online</option>
                <option value="ambos">ambos</option>
            </select>
    </div>
    <!-- boton submit,al hacer click aqui se aplicaran los filtros-->
    <input class="col-lg-2 col-md-3 col-7 offset-1 container2 button" id='submit' name='submit' type= "submit" value="buscar">
    </div>
    </div>

</form>
<!-- esta es la tabla donde el administrador vera la toda informacion de la base de datos, 
al hacer click en el nombre o en la imagen ,entraran en la pagina detallada
tambien hay botones para insertar datos, editarlos y borrarlos-->

    <div class ="tabla col-12 container2 ">
        <!--boton para insertar entrada en la base de datos-->
        <a  class='button tableButton' href="../paneladmin/insertar/newJuego_Controller.php"><?='insertar'?></a>
        <!--gestion de reportes-->
        <a  class='button tableButton' id="reportes" href="../paneladmin/gestion_de_reportes/report_list_controller.php"><?='gestionar reportes'?></a>
        <!--restablecer los filtros-->
        <a  class='button tableButton' id="restablecerFiltros" href="../paneladmin/adminlistJuego_controller.php"><?='restablecer filtros'?></a>
        <!--decidimos el tamaño horizontal de la tabla y de cada columna, tambien habra barras de desplazamiento lateral y vertical-->
        <div style="overflow:scroll;height:500px; " class="col-12">
                <div  class ="col-12  container2"  id='mensaje'>
                    <table class ="container2"  style="width:255%">
                        <col style="width:2%">
                        <col style="width:2%">
                        <col style="width:10%">
                        <col style="width:10%">
                        <col style="width:10%">
                        <col style="width:5%">
                        <col style="width:5%">
                        <col style="width:5%">
                        <col style="width:5%">
                        <col style="width:20%">
                        <col style="width:41%">
                        <col style="width:27%">
                        <col style="width:27%">
                        <thead>
                                <tr>
                                <th></th> <th></th> <th>imagen</th><th>imagen2 </th><th>imagen3</th><th>nombre</th><th>desarrollador</th><th>genero </th><th>multijugador </th><th>descripcion corta</th><th>descripcion larga</th><th>video 1</th><th>video2</th>
                                </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($juegos as $juego ):?>
                                <tr>
                                    <!--botones para editar y borrar-->
                                    <td class="<?=$juego["descripcionCorta"]."1" ?>"  >
                                    <a class='button' href="../paneladmin/editar/editJuego_controller.php?nombre=<?=urlencode($juego['nombre'])?>"> <?='editar'?></a></td>
                                    <td class="<?=$juego['descripcionLarga']."1" ?>" >
                                    <a class='button' href="../paneladmin/borrar/deleteConfirm_controller.php?nombre=<?=urlencode($juego['nombre'])?>"> <?='borrar'?></a></td>
                                    <!--imagenes-->
                                    <td><a href="../funciones_comunes/pagina_detallada/detallada_controller.php?nombre=<?=urlencode($juego['nombre'])?>">  
                                        <img src="../getimages/getimage.php?nombre=<?=urlencode($juego['nombre'])?>" width="100%" height="150px"></a></td>
                                    <td><a href="../funciones_comunes/pagina_detallada/detallada_controller.php?nombre=<?=urlencode($juego['nombre'])?>">  
                                        <img src="../getimages/getimage2.php?nombre=<?=urlencode($juego['nombre'])?>" width="100%" height="150px"> </td>
                                    <td><a href="../funciones_comunes/pagina_detallada/detallada_controller.php?nombre=<?=urlencode($juego['nombre'])?>">   
                                        <img src="../getimages/getimage3.php?nombre=<?=urlencode($juego['nombre'])?>" width="100%" height="150px"> </td>
                                    <!--datos-->
                                    <td class="<?=$juego["nombre"]?>">
                                    <a href="../funciones_comunes/pagina_detallada/detallada_controller.php?nombre=<?=urlencode($juego['nombre'])?>">  <?=$juego["nombre"]?></td>
                                    <td class="<?=$juego["desarrollador"]?>"  ><?=$juego["desarrollador"] ?> </td>
                                    <td class="<?=$juego["genero"] ?>" > <?=$juego["genero"] ?></td>
                                    <td > <?=$juego["multijugador"] ?> </td>
                                    <td class="<?=$juego["descripcionCorta"] ?>"><?=$juego["descripcionCorta"] ?> </td>
                                    <td class="<?=$juego['descripcionLarga']?>" ><?=$juego["descripcionLarga"] ?></td> 
                                    <!--videos-->
                                    <td> <iframe width="300" height="250" src="https://www.youtube.com/embed/<?=$juego["codigoVideoYoutube"]?>"frameborder="0" allowfullscreen></iframe></td> 
                                    <td> <iframe width="300" height="250" src="https://www.youtube.com/embed/<?=$juego["codigoVideoYoutube2"]?>"frameborder="0" allowfullscreen></iframe></td> 
                                
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
<?php }else { 
header ("location:index.php");
}?>
</body>
</html>
