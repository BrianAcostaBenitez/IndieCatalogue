<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../factoryConnection.php";
require_once "../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../config.php";

session_start();


if (isset($_SESSION['username']) ){
try {
  $factory = new FactoryConnection($config);
  $repository = new PDOJuegoRepository($factory->get());
  //llamamos a las querys que dan todos los datos y la de los generos, esa informacion estara  en el panel de administrador
  $juegos=$repository->getAll();
  $generos = $repository->getAllGenres();
  // y cargamos el panel de administrador
  require "paneladmin.php";
  if (is_null($juegos)){
    ?>
    <script>
          document.getElementById("mensaje").innerHTML ='No se encontraron resultados ';
    </script>
    <?php
  }  
} catch (PDOException $e) {
  print "¡Error!:" . $e->getMessage() . "<br/>";
  die();
} finally {
  $repository = null;

}
}
else { 
  header ("location:index.php");
}