<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../factoryConnection.php";
require_once "../../funciones_comunes/reportes/pdoReportesRepository.php";
require_once "../../funciones_comunes/comentarios/pdoComentariosRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../../config.php";

session_start();
if (isset($_SESSION['username']) ){
try {
  $factory = new FactoryConnection($config);
  $reportRepository = new PDOReportesRepository($factory->get());
  //llamamos a las querys que da los datos de los comentarios reportados
  $reportes= $reportRepository->getReportedComments();
  //cargamos la pagina de los reportes
  require "reportList.php";
  //si no hay reportes se nos mostrara un mensaje que lo indique
  if (is_null($reportes)){
    ?>
    <script>
          document.getElementById("mensaje").innerHTML ='No hay ningun reporte, todo en orden ';
    </script>
    <?php
  } 
} catch (PDOException $e) {
  print "¡Error!:" . $e->getMessage() . "<br/>";
  die();
} finally {
  $repository = null;

}
}
else { 
  header ("location:../index.php");
}