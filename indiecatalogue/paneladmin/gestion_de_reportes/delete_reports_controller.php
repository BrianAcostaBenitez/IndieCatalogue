<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../factoryConnection.php";
require_once "../../funciones_comunes/reportes/pdoReportesRepository.php";
require_once "../../funciones_comunes/comentarios/pdoComentariosRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../../config.php";
//recogemos la id del comentario reportado
$id = $_GET["id"];
try {
    session_start(); 
    if (isset($_SESSION['username']) ){
    header ("location:report_list_controller.php");
    $factory = new FactoryConnection($config);
    $reportRepository = new PDOReportesRepository($factory->get());
    //se borraran los reportes del comentario
    $reportRepository->deleteReports($id);
    //volvemos a la lista de reportes
}else { 
    header ("location:../index.php");
}
} catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $reportRepository = null;
}
