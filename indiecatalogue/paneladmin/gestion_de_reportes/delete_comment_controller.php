<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../factoryConnection.php";
require_once "../../funciones_comunes/reportes/pdoReportesRepository.php";
require_once "../../funciones_comunes/comentarios/pdoComentariosRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../../config.php";
//recogemos el nombre del comentario reportado
$id = $_GET["id"];
try {
    session_start(); 
    if (isset($_SESSION['username']) ){
    $factory = new FactoryConnection($config);
    $reportRepository = new PDOComentariosRepository($factory->get());
    //se borrara el comentario junto con sus reportes
    $reportRepository->deleteComment($id);
    //volvemos a la lista de reportes
    header ("location:report_list_controller.php");
}else { 
    header ("location:../index.php");
}
} catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $reportRepository = null;
}
