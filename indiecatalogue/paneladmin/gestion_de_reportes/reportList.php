<!doctype html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content ="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="reportList.css">

<title>indiecatalogue panel admin</title>

</head>

<body>
<?php

    if (isset($_SESSION['username']) ){
?>
<div class="row">
    <div class="container1 col-10 offset-1" >
   
    <div class="texto2" ><p>aqui se encuentran los reportes de los comentarios, junto al motivo del reporte, por favor, borre los comentarios ofensivos</p>
    <a class='button col-5' id='volver' href="../adminlistJuego_controller.php"> <?='volver'?></a></div>
    </div> 
</div>
<!-- esta es la tabla donde el administrador vera los reportes
podemos elegir borrar solo los reportes de un comentario, o borrar el comentario,lo cual borrara de paso los reportes-->
<div style="overflow:scroll;height:500px; " class="col-12">
<div class ="tabla container2 col-10 offset-1 ">
        <div class ="container2 " id='mensaje'>
<table class ="container2"  style="width:100%">
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
<col style="width:25%">
<thead>
        <tr>
        <th></th><th></th><th>motivo del reporte</th><th>comentario</th>
        </tr>
</thead>
<tbody>
<?php foreach ($reportes as $reporte ):?>
    <tr>
        <!-- borrar reportes y "absolver" comentario-->
        <td ><a class='button col-12 <?=$reporte["comentario"].'reporte'?>' href="delete_reports_controller.php?id=<?=urlencode($reporte['id'])?>"> <?='eliminar reportes del comentario'?></a></td>
        <!-- borar comentario, lo cual de paso borra los reportes del mismo-->
        <td><a class='button col-12 <?=$reporte["comentario"].'comentario'?>' href="delete_comment_controller.php?id=<?=urlencode($reporte['id'])?>"> <?='eliminar comentario y reportes'?></a></td>
        <td><?=$reporte["motivo"]?></td> 
        <td class="<?=$reporte["comentario"]."comprobacion"?>"><?=$reporte["comentario"]?></td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
</div>
</div>
<?php }else { 
header ("location:../index.php");
}?>
</body>
</html>
