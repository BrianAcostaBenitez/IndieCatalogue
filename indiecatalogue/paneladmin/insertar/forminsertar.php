<!DOCTYPE html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content ="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
<link rel="stylesheet" href="forminsertar.css">
<title>insertar</title>
<style>
</style>
</head>
<body>
<?php

        if (isset($_SESSION['username']) ){
?>
<form action="../adminlistJuego_controller.php" >
    <input type= "submit" class="float-left submit-button container2" id='button' value="volver">
</form>
 <!--formulario para insertar datos, las imagenes se pueden enviar a la base de datos a traves de un boton-->
<div class="row justify-content-around">
<div class="container1 col-lg-10 col-12"> 
<!--si ponemos un nombre duplicado entonces se nos mostrara que no podremos hacer la insercion-->
        <p id="mensaje"></p>
        <form action="addJuego_Controller.php" method="post" enctype="multipart/form-data" >
        <div class="row justify-content-around">
                <!--campo nombre-->
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="nombre"> nombre</label>
                        <input type="text" id="nombre" name="nombre" required>
                </div>
                        <!--imagenes-->
                <div class="container2  col-lg-5 col-md-6 col-12">
                <label for="image"> imagen</label>
                <input type="file" name="image" id="image" required>
                </div>
        </div>
        <div class="row justify-content-around">
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="image2"> imagen2</label>
                        <input type="file" name="image2" id="image2" required>
                </div>
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="image3"> imagen3</label>
                        <input type="file"  name="image3" id="image3" required>
                </div>
        </div>
        <div class="row justify-content-around">
                <!--campo desarrollador-->
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="desarrollador">desarrollador</label>
                        <input type="text" id="desarrollador" name="desarrollador" required>
                </div>
                <!--campo genero ,las opciones estan sacadas de la base de datos-->
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="genero">genero</label>
                        <select name=genero id="genero" class="genero">
                                <?php foreach($generos as $genero): ?>
                                        <option value="<?=$genero['genero']?>">
                                        <?= $genero['genero']?>
                                        </option>
                                <?php endforeach;?>
                        </select>
                </div>
        </div>
        <div class="row justify-content-around">
                <!--campo multijugador-->
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="multijugador">multijugador</label>
                                <select name=multijugador id="multijugador">
                                        <option value="no tiene">no tiene</option>
                                        <option value="offline">offline</option>
                                        <option value="online">online</option>
                                        <option value="ambos">ambos</option>
                                </select>
                </div>
                <!--campo descripcion corta-->
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="descCorta">descripcion corta</label>
                        <textarea rows="2"  cols="70" style="max-width:100%;" name="descripcionCorta" id="descripcionCorta"  maxlength="254"></textarea>
                </div>
        </div>
        <div class="row justify-content-around">
                 <!--descripcion larga-->
                 <div class="container2 col-12">
                <label for="descLarga">descripcion larga</label>
                <textarea rows="4" cols="120" style="max-width:100%;"  name="descripcionLarga" id="descripcionLarga"  maxlength="1500"></textarea>
                </div>
        </div>
         <!--codigo de los videos-->
         <div class="row justify-content-around">
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="codigoVideoYoutube"> codigo Video Youtube</label>
                        <input type="text" id="codigoVideoYoutube" name="codigoVideoYoutube" required>
                </div>
                <div class="container2  col-lg-5 col-md-6 col-12">
                        <label for="codigoVideoYoutube2"> codigo Video Youtube</label>
                        <input type="text" id="codigoVideoYoutube2" name="codigoVideoYoutube2" required>
                </div>
        </div>
         <!--boton submit-->
        <input type= "submit" class=" float-left submit-button container2" id='button' value="guardar">
</div>  
</div>    
<?php }else { 
    header ("location:../index.php");
}?>
</body>
</html>