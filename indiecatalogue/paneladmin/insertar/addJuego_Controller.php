<?php 
namespace Brian\IndieCatalogue\Juego;
session_start();
error_reporting(E_ERROR | E_PARSE);
require_once "../../juego.php";
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
if (isset($_SESSION['username']) ){
  $config = require_once "../../config.php";
  $factory = new FactoryConnection($config);
  $repository = new PDOJuegoRepository($factory->get());
   //nos aseguramos que de no ponemos un nombre duplicado
   $nombre = $_POST["nombre"];
   $searchnombre=$repository->getEx($nombre);
   $checknombre= $searchnombre->nombre;
   //si el nombre esta duplicado entonces no podremos hacer la insercion y saldra un mensaje que nos lo dira
   if ($nombre == $checknombre) {
    $generos = $repository->getAllGenres();
    require ("forminsertar.php");
   ?>
   <script>document.getElementById("mensaje").innerHTML ='Nombre duplicado no se puede meter el mismo juego 2 veces';
   </script>
   <?php
    }else{
      ?>
      <script type="text/javascript">
      window.location.href = '../adminlistJuego_controller.php';
      </script>
      <?php
    //rellenamos los atributos del objeto juego con los datos del formulario
    $juego = new Juego($_POST["nombre"], $_POST["desarrollador"],$_POST["genero"],$_POST["multijugador"],
    $_POST["descripcionCorta"] , $_POST["descripcionLarga"],$_POST["codigoVideoYoutube"],$_POST["codigoVideoYoutube2"] ,date("Y/m/d")
    );
    //las imagenes las sacamos aqui, luego enviamos todo junto a la query
    $image = file_get_contents($_FILES['image']['tmp_name']);
    $image2 = file_get_contents($_FILES['image2']['tmp_name']);
    $image3 = file_get_contents($_FILES['image3']['tmp_name']);
    $repository->new($juego,$image,$image2,$image3);
    
}}else { 
  header ("Location: ../index.php");
}