<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../juego.php";
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../../config.php";
$factory = new FactoryConnection($config);
        $conn = $factory->get();
try {
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }

    if (isset($_SESSION['username']) ){
        //creamos una instancia del objeto juego ,el cual rellenaremos con los datos del form
        $juego = new Juego("", "", "","","","","","","");
        $repository = new PDOJuegoRepository($conn);
        //necesitaremos sacar la lista de generos para un select
        $generos = $repository->getAllGenres();
        require "forminsertar.php";
    }
    else { 
    header ("Location: ../index.php");
    }
} catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $repository = null;
}