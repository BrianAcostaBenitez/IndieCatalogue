<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
 require_once "../factoryConnection.php";
require_once "../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../config.php";
//Se cogen los datos del form del index, si el usuario y contraseña introducidos
//coinciden con una entrada en la base de datos se podra acceder al panel de administrador
try {
    session_start(); 
    $factory = new FactoryConnection($config);
    $repository = new PDOJuegoRepository($factory->get());
    $username=$_POST["usuario"];
    $username= strval($username);
    $passwd=$_POST['passwd'];
    $passwd= strval($passwd);
    $admin = $repository->getadmin($username, $passwd);

if ($admin[0] === $username && $admin[1] === $passwd) {
    $_SESSION['username'] = $admin[0];
    header ("location:adminlistJuego_controller.php");
   }
else{
    require "index.php";
 ?>
<script>
      document.getElementById("mensaje").innerHTML ='Usuario o contraseña incorrectas';
</script>
<?php
}
} catch (PDOException $e) {
print "¡Error!:" . $e->getMessage() . "<br/>";
die();
} finally {
    $repository = null;
}
