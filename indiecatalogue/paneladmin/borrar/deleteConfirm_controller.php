<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
require_once "../../juego.php";
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../../config.php";
try {
    session_start(); 
    if (isset($_SESSION['username']) ){
        //cogemos los datos del juego escogido,necesitamos el nombre ,luego cargamos el mensaje de confirmacion
    $factory = new FactoryConnection($config);
    $repository =  new PDOJuegoRepository($factory->get());
    $juego = $repository->getEx($_GET["nombre"]);
 
    require "confirmacionborrar.php";
}else { 
    header ("Location: ../index.php");
}
} catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $repository = null;
}