<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
session_start(); 
require_once "../../juego.php";
require_once "../../factoryConnection.php";
require_once "../../pdoJuegoRepository.php";
 
use \Brian\IndieCatalogue\FactoryConnection;
$config = require_once "../../config.php";
//recogemos el nombre del juego 
$juego = $_POST["nombre"];
try {
    if (isset($_SESSION['username']) ){
    $factory = new FactoryConnection($config);
    $repository = new PDOJuegoRepository($factory->get());
    //si hemos confirmado que queremos borrar la entrada, se borrara
    $repository->delete($juego);
    header ("location:../adminlistJuego_controller.php");
}else { 
    header ("Location: ../index.php");
}
} catch (PDOException $e) {
    print "¡error!:" . $e->getMessage() . "<br/>";
    die();
} finally {
    $repository = null;
}
