<!DOCTYPE html>

<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content ="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="../../css/bootstrap.min.css">
<link rel="stylesheet" href="confirmacionborrar.css">
<!-- media queries: permite definir estilos personalizados si se cumples las condiciones establecidas
	-->
	<style type="text/css">
	@media	(max-width:537px)	
	{
		.container1
		{
			height: 300px;
		}		
	}		
    </style>
<title>borrar</title>
</head>
<body>
<?php
    if (isset($_SESSION['username']) ){
?>
 <!--un simple mensaje de confirmacion, si queremos borrar la entrada confirmamos que si-->
<div class="row justify-content-around">
<div class="container1 col-10 ">
    <div class="row justify-content-around">
        <div class="texto1 col-12"><p>¿Esta seguro que quiere borrar la siguiente entrada?: <?=$juego->nombre?> si cambia de opinion, debera reintroducir los datos</p></div>
            <form action="../adminlistJuego_controller.php" >
                <button class="submit-button denegar col-lg-12 col-md-12 col-12" id="button">No, volver al panel de administrador</button>
            </form>
            <form action="deleteGame_controller.php" method="post" enctype="multipart/form-data" >
                <!--aqui tenemos un input escondido con el nombre del juego que hemos escogido, para que asi se borre la entrada deseada-->
                <input type="hidden" id="nombre" name="nombre" value="<?=$juego->nombre?>">
            <button class="submit-button confirmar col-lg-12 col-md-12 col-12" id="button">Si, borrar la entrada</button>
            </form>   
    </div>
</div>
</div>

<?php 
}else { 
    header ("Location: ../index.php");
  }?>
</body>
</html>