<?php declare(strict_types=1);
namespace Brian\IndieCatalogue\Juego;
//controlador para coger la primera imagen de la base de datos
require_once "../juego.php";
require_once "../factoryConnection.php";
require_once "../pdoJuegoRepository.php";
use \Brian\IndieCatalogue\FactoryConnection as FactoryConnection;
$config = require_once "../config.php";
$factory = new FactoryConnection($config);
$repository =  new PDOJuegoRepository($factory->get());
$imagen = $repository->getImage($_GET["nombre"]);
echo $imagen;
   