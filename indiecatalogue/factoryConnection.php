<?php declare(strict_types=1);  
namespace Brian\IndieCatalogue;



class FactoryConnection {
    private $config;
    function __construct($config) {
        $this->config = $config;
    }
    //conexion, hace falta hacer requires en los controladores para que usen esto, el archivo config.php y el archivo pdoJuegoRepository y asi funcionen
    function get(): \PDO {
        return new \pdo("{$this->config['type']}:host={$this->config['host']};dbname={$this->config['dbname']}",
        $this->config['user'], $this->config["password"]);
    }
    }