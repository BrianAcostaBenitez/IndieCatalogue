# IndieCatalogue




## Descripcion
Muchos jugadores de videojuegos disfrutan de los mismos ,no obstante, a veces le pueden apetecer algo distinto a los juegos de alto presupuesto como lo podrían ser Súper Mario, Fifa o Call of duty, los videojuegos indie suelen ser adecuados , no obstante hay un problema, en una tienda digital fácilmente pueden salir a la venta cientos de juegos en un solo día,esto incluye a los indies, y muchos suelen ser productos de baja calidad realizados a las prisas con la intención de conseguir beneficios fáciles y rápidos, un catalogo donde solo se muestren juegos indie de calidad como lo sera el nuestro sera de ayuda a muchas personas 
Dicho catalogo se podrá actualizar con el tiempo para incluir mas y mas juegos, como función extra ,se usara la api cheapshark para obtener las ofertas de dichos juegos, asistiendo al usuario a encontrar un buen precio
Este trabajo usa una licencia creative commons, se permite descargarlo y mostrarlo siempre y cuando se reconozca la autoría



## Instalacion
En primer lugar debemos ir a la configuración de MySQL en el xampp, por defecto en “max_allowed_packet” el valor sera 1 pero lo cambiamos a un valor mayor, por ejemplo 1000 esto hay que hacerlo para luego no tener problemas al insertar la base de datos por ser mas grande de lo que esta especificado en  “max_allowed_packet”.

Luego tenemos que crear una base de datos, de nombre “indiecatalogue”.
Después, desde la base de datos recién creada vamos a “importar” , quitamos el “check” a las opciones en los recuadros. Tambien tenemos que seleccionar el archivo sql que esta incluido en la entrega y tras hacer click en el botón continuar, se importaran los datos.


A continuación vamos a Cuentas de usuarios y hacemos click en “agregar cuentas de usuario” ya que necesitamos crear el usuario con el que la pagina se conectara a la base de datos

Rellenaremos el formulario debemos poner usuario admin, contraseña 1234 y al nombre del host le ponemos la opción “local”,También le daremos todos los permisos.

Ahora pondremos la carpeta del proyecto en el directorio htdocs en xampp y de ese modo nos sera posible acceder a la pagina de manera local siempre y cuando tengamos xampp activado.
No obstante, esto no basta para que podamos ejecutar los tests automatizados, para eso falta hacer 2 cosas mas,  para poder ejecutar los test automatizados,tendremos que realizar los siguientes pasos

Primero debemos instalar node.js en la pagina web oficial elegimos la versión recomendada para la mayoría, al instalarlo hacemos click en Next hasta que termine la instalación ,como con casi cualquier otro programa que podamos instalar en windows 


Cuando termine la instalación hay que reiniciar el equipo o maquina virtual que estemos usando, los siguiente pasos no en teoría harán falta hacerlos si tenemos el directorio de módulos y los archivos de playwright tal y como vienen entregados si desea borrarlos para probar que estos pasos funcionan, primero copie la carpeta de los tests en otro lugar porque ahí esta el código de los tests, tampoco borre la carpeta “imágenes” porque ahí están las imágenes que se usan en los test de las acciones CRUD y la imagen de la licencia creative commons
Luego abrimos visual code studio y en File seleccionamos Open Folder para abrir la carpeta del proyecto (indiecatalogue), si no hacemos este paso ,los siguientes no nos sirven de nada, pues los comandos necesitan un “objetivo” donde ejecutarse ,y de este modo se lo damos sin tener que especificar la carpeta del proyecto en cada comando, ademas de que hace falta para ejecutar los tests
 Instalamos playwright en el proyecto con el comando “npm init playwright@latest” cuando nos lo pida ,introducimos “y”, después nos preguntara si queremos usar Typescript o Javascript, elegimos lo segundo y por ultimo nos preguntara si queremos añadir un workflow de actions de github, yo puse que no.
Ahora tocaremos la configuración de playwright, queremos que los test se ejecuten en un solo navegador, ya que algunos test hacen acciones crud, como tratan de meter los mismos datos y hacer las mismas acciones, entonces solo un navegador tendrá éxito ya que no permitimos entradas duplicadas ,ademas, nos interesa aumentar el tiempo limite que  se les da a los test para realizarse

Ahora volvemos a dejar el directorio de los test que anteriormente copiamos donde estaba antes, no obstante hay que tener en cuenta que playwirght nos crea un archivo de test llamado “example.spec.js” para que tengamos un ejemplo de como hacerlos,podemos ignorarlo o borrarlo
Al intentar realizar un test es posible que nos salga este mensaje, ejecutamos el comando que nos pide (npx playwright install), esto descarga versiones de navegadores que playwright usa para los test automatizados





## Licencia

Este trabajo usa una licencia creative commons, se permite descargarlo y mostrarlo siempre y cuando se reconozca la autoría
